"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/6/13 20:43
@Author  : 派大星
@Site    : 
@File    : api.py
@Software: PyCharm
@desc:
"""
import logging
import requests


class Api:
    """封装一下 get、post 请求"""
    def __init__(self, base_url):
        self.base_url = base_url
        self.session = requests.session()

    def get(self, url, **kwargs):
        """封装一下 get 请求"""
        logging.info(f'发送 GET 请求 {url} {kwargs}')
        if not url.startswith('http'):
            url = self.base_url + url
        res = self.session.get(url, **kwargs)
        logging.info(f'响应数据为 {res.text}')
        return res

    def post(self, url, **kwargs):
        """封装一下 post 请求"""
        logging.info(f'发送 POST 请求 {url} {kwargs}')
        if not url.startswith('http'):
            url = self.base_url + url
        res = self.session.post(url, **kwargs)
        logging.info(f'响应数据为 {res.text}')
        return res
