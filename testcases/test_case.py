"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/6/13 21:53
@Author  : 派大星
@Site    : 
@File    : test_case.py
@Software: PyCharm
@desc:
"""
import json
import logging

import pytest
import requests
from utils.clean_data import clean_data
from utils.parse_excel import write_excel
from utils.check_response import check_status_res_code

case_list = clean_data()


@pytest.mark.parametrize('test_data', case_list)
def test_case(base_api, test_data):
    """测试用例"""
    # 实例化 Api 对象
    case_id = test_data.get('case_id')
    case_tile = test_data.get('case_tile')
    url = test_data.get('url')
    method = test_data.get('method')
    headers = test_data.get('headers')
    data = test_data.get('data')
    json_data = test_data.get('json_data')
    files = test_data.get('files')
    extract = test_data.get('extract')
    _assert = test_data.get('assert')
    if method.lower() == 'get':
        response = base_api.get(url, headers=headers, params=data)
        # check_status_res_code(response, extract, _assert)
        if response.status_code == 200:
            write_excel(case_id)
        else:
            write_excel(row=case_id, value='FAIL')
    elif method.lower() == 'post':
        response = base_api.post(url, headers=headers, data=data, json=json_data, files=files)
        # check_status_res_code(response, extract, _assert)
        if response.status_code == 200:
            write_excel(case_id)
        else:
            write_excel(row=case_id, value='FAIL')


if __name__ == '__main__':
    pytest.main(['-s', 'test_case.py' '--html=reports/report.html --self-contained-html'])
