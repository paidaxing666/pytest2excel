"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/6/13 21:56
@Author  : 派大星
@Site    : 
@File    : conftest.py
@Software: PyCharm
@desc:
"""
import pytest
from API.api import Api


@pytest.fixture(scope='session')
def base_api(base_url):
    """实例化 Api 对象"""
    base_api = Api(base_url)
    return base_api
