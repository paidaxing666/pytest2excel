"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/6/14 11:10
@Author  : 派大星
@Site    : 
@File    : clean_data.py
@Software: PyCharm
@desc:
"""
import json
from utils.parse_excel import read_excel


def clean_data():
    """清理并格式化数据"""
    case_list = []

    test_data = read_excel()
    for index, _data in enumerate(test_data):
        case_dict = {}
        if _data[0] is not None and _data[1] is not None and _data[2] is not None and _data[3] is not None:
            # 判断前四个必填项是否为空
            case_dict.update({'case_id': index+2, 'case_tile': _data[1], 'url': _data[2], 'method': _data[3]})
        if _data[4] is None:  # 判断headers是否为空
            case_dict.update({'headers': {}})
        else:  # 拼接headers，转为字典
            case_dict.update({'headers': {item.split(':')[0].strip(): item.split(':')[1].strip() for item in _data[4].split('\n')}})
        _data_5 = _data[5]
        case_dict.update({'data': {}, 'json_data': {}, 'files': {}})
        if _data_5 is None:  # 判断data是否为空
            pass
        elif _data_5.strip().startswith('{') or _data_5.strip().startswith('['):  # 判断data是否为json格式
            case_dict.update({'json_data': json.loads(_data_5)})
        else:  # 判断data是否为表单格式
            for value_list in _data_5.split('\n'):
                data_key, data_value = value_list.split(':')
                if data_value.strip().startswith('<'):  # 判断data是否为文件上传，headers添加Content-Type
                    case_dict['headers'].update({'Content-Type': 'multipart/form-data'})
                    case_dict['files'].update({data_key: data_value})
                else:
                    case_dict['data'].update({data_key: data_value})

        if _data[6] is None:  # 判断提取变量为空
            case_dict.update({'extract': {}})
        else:  # 拼接提取变量，转为字典
            pass
            case_dict.update({'extract': _data[6].split('\n')})
        if _data[7] is None:  # 判断断言为空
            case_dict.update({'assert': {}})
        else:  # 拼接断言，转为字典
            case_dict.update({'assert': _data[7].split('\n')})
        case_list.append(case_dict)
    print(case_list)
    return case_list


if __name__ == '__main__':
    clean_data()
