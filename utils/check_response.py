"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/6/15 17:01
@Author  : 派大星
@Site    : 
@File    : check_response.py
@Software: PyCharm
@desc:
"""
from string import Template

# 定义全局变量，用于储存键值对
global_data = {}


def set_global_data(key, value):
    """设置全局变量"""
    global global_data
    global_data[key] = value


def get_global_data(key):
    """返回全局变量值"""
    return global_data.get(key)


def is_dynamic_parameter(response, extract_assert_str):
    """判断是否为动态参数"""
    if extract_assert_str.startswith('$'):
        if 'response' in extract_assert_str:
            return eval(extract_assert_str.strip().strip('$'))
        else:
            if '[' in extract_assert_str or '.get' in extract_assert_str:
                inter_variable = extract_assert_str.split('[')[0] if '[' in extract_assert_str \
                    else extract_assert_str.split('.get')[0]
                inter_result = get_global_data(inter_variable.strip().strip('$'))
                return eval(str(inter_result) + extract_assert_str.strip().strip('$').replace(inter_variable.strip().strip('$'), ''))
            else:
                return get_global_data(extract_assert_str.strip().strip('$'))
    else:
        return extract_assert_str


def check_status_res_code(response, extract, _assert):
    """检查响应状态码"""
    # print(response)
    # print(extract)
    # print(_assert)
    if extract:
        for _extract in extract:
            _key, _path = _extract.split('=')
            set_global_data(_key, eval(_path.strip().strip('$')))

            _key = is_dynamic_parameter(response, _path)
    if _assert:

        for _ass in _assert:
            if '==' in _ass:
                _assert_key, _value = _ass.split('==')

                _assert_key = is_dynamic_parameter(response, _assert_key)
                _value = is_dynamic_parameter(response, _value)
                if _assert_key != _value:
                    return False
            else:
                if eval(Template(_ass).safe_substitute()):
                    return True
        return True
