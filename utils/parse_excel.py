"""
!/usr/bin/env python
# -*- coding: utf-8 -*-
@Time    : 2023/6/13 22:04
@Author  : 派大星
@Site    : 
@File    : parse_excel.py
@Software: PyCharm
@desc:
"""
import json
import os
from openpyxl import load_workbook
from openpyxl.styles import PatternFill


ROOT_PATH = os.path.dirname(os.path.dirname(__file__))
excel_path = os.path.join(ROOT_PATH, 'data', '接口示例.xlsx')
wb = load_workbook(excel_path)
ws = wb.active


def read_excel():
    """读取Excel"""
    test_data = []
    for row in ws.iter_rows(min_row=2, max_row=ws.max_row, min_col=1, max_col=ws.max_column):
        test_data.append([cell.value for cell in row])
    return test_data


def write_excel(row, column=10, value='PASS'):
    """写入Excel"""
    ws.cell(row=row, column=column).value = value
    if value == 'PASS':
        ws.cell(row=row, column=column).fill = PatternFill("solid", fgColor="00FF00")
    elif value == 'FAIL':
        ws.cell(row=row, column=column).fill = PatternFill("solid", fgColor="FF0000")
    else:
        ws.cell(row=row, column=column).fill = PatternFill("solid", fgColor="FFFF93")
    wb.save(excel_path)


if __name__ == '__main__':
    print(read_excel())
